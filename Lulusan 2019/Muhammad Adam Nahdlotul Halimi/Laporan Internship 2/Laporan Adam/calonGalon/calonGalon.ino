#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3, POSITIVE);

volatile int putar;
int Calc;
int s1 = 6;//non-contact
int s2 = 2;//waterflow
int outH = 5;
int outK = 4;
int outM = 3;

int nAtas = 88;
int nTengah = 56;
int nBawah = 8;

void rpm (){
  putar++;
}

void setup() {
  // put your setup code here, to run once:
  pinMode (s1, INPUT);
  pinMode (s2, INPUT);
  pinMode (outM, OUTPUT);
  pinMode (outK, OUTPUT);
  pinMode (outH, OUTPUT);
  attachInterrupt(0, rpm, RISING);
  lcd.begin (16,2);

  
}

void loop() {
  // put your main code here, to run repeatedly:
  putar = 0;
  sei();
  delay(1000);
  
  cli();
  Calc = (putar * 60 / 7.5);
  sei();
  
  lcd.setBacklight(HIGH);
  lcd.setCursor(0,0);
  lcd.print("Flow Rate:");
//  lcd.setCursor(12,0);
  lcd.print(Calc, DEC);
  
  int Liquid_level = 1;

  Liquid_level = digitalRead(6);
  
  if ((Liquid_level == 1) && (Calc >= nTengah)){
    digitalWrite (outH, HIGH);
    digitalWrite (outK, LOW);
    digitalWrite (outM, LOW);
    //lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("PENUH!");
    //delay(1000);
    lcd.setBacklight(HIGH);
    //delay(1000);
       
  } 
  delay(100);
  if ((Liquid_level == 1) && (Calc <= nTengah)){
    digitalWrite (outH, LOW);
    digitalWrite (outK, HIGH);
    digitalWrite (outM, LOW);
    //lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("SEDANG");
    //delay(1000);
    lcd.setBacklight(HIGH);
    //delay(1000);
    
  } 
  delay(100);
  if ((Liquid_level == 0) && (Calc <= nTengah)){
    digitalWrite (outH, LOW);
    digitalWrite (outK, LOW);
    digitalWrite (outM, HIGH);
    //lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("KOSONG");
    //delay(1000);
    lcd.setBacklight(HIGH);
    //delay(1000);
  }
  delay(100);

 
  cli();
  Calc = (putar * 60 / 7.5);
  //delay(1000);
    
  
 }
